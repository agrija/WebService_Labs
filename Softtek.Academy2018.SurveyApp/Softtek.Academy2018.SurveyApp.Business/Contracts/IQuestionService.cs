﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface IquestionService
    {
        int Add(Question question);

        bool Delete(int id);

        Question Get(int id);

        ICollection<Question> GetAll(); //All questions in a survey

        bool Update(Question question);
    }
}
