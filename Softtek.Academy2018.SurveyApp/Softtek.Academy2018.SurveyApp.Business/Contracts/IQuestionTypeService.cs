﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface IQuestionService
    {
        int Add(Question question);

        bool Delete(int id);

        Question Get(int id);

        //Get all questions from a survey method
        ICollection<Question> GetAll();

        //According to diagram, question can have multiple options
        bool AddOption(int questionId, int optionId);
        bool DeleteOption(int questionId, int optionId);

        //CHECK: What other methods go here
        ICollection<Option> GetOptionbyQuestion(int questionId);
    }
}
