﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    //Survey Methods: AddQuestion, AddOption to survey 
    public interface ISurveyService
    {
        int Add(Survey survey);

        bool Delete(int id);

        Survey Get(int id);

        bool Update(Survey item);

        //Add question to survey
        bool AddQuestion(int questionId, int surveyId);

        //According to the diagram. Question-Survey have relation

        //Delete question from sruevy
        bool DeleteQuestion(int questionId, int surveyId);
    }
}
