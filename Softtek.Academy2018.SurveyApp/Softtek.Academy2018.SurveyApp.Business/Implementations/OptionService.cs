﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    //Bussiness contract
    public class OptionService : IOptionService
    {
        //Aquí mandamos llamar al contrato del DATA
        private readonly IOptionRepository _optionRepository;

        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        //Add method
        public int Add(Option option)
        {
            if (option == null) return 0;   //Zero meaning bad request

            if (string.IsNullOrEmpty(option.Text)) return 0;

            //if (option.Colaborators.Count != 0) return 0;

            //Verify option is not existent     
            bool optionExists = _optionRepository.OptionExists(option.Text);

            //If true, return 0
            if (optionExists) return 0;

            int id = _optionRepository.Add(option);

            return id;
        }

        //Delete method
        public bool Delete(int id)
        {
            if (id <= 0) return false;

            Option option = _optionRepository.Get(id);
            if (option == null) return false;

            //Check if it is assigned to a question
            bool optionIsAssigned = _optionRepository.OptionIsAssigned(id);
            if (optionIsAssigned) return false;

            return _optionRepository.Delete(id);
        }

        //Get by Id method
        public Option Get(int id)
        {
            if (id <= 0) return null;

            Option option = _optionRepository.Get(id);  //Get it by id

            if (option != null) return null;

            return option;
        }

        public ICollection<Option> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Update(Option option)
        {
            if (option.Id <= 0) return false;

            if (string.IsNullOrEmpty(option.Text)) return false;

            Option currentId = _optionRepository.Get(option.Id);
            if (currentId == null) return false;

            bool optionExists = _optionRepository.OptionExists(option.Text);
            if (optionExists || currentId.Id != option.Id) return false;

            bool optionIsAssigned = _optionRepository.OptionIsAssigned(option.Id);
            if (optionIsAssigned) return false;

            return _optionRepository.Update(option);
        }
    }
}
