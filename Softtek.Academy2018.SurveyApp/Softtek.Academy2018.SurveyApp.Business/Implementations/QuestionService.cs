﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class QuestionService : IQuestionService
    {

        private readonly IQuestionRepository _optionRepository;

        public OptionService(IQuestionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        public int Add(Question question)
        {
            throw new NotImplementedException();
        }

        public bool AddOption(int questionId, int optionId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteOption(int questionId, int optionId)
        {
            throw new NotImplementedException();
        }

        public Question Get(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Question> GetAll()
        {
            throw new NotImplementedException();
        }

        public ICollection<Option> GetOptionbyQuestion(int questionId)
        {
            throw new NotImplementedException();
        }
    }
}
