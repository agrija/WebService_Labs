﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option option)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Option Get(int optionId)
        {
            throw new NotImplementedException();
        }

        public ICollection<Option> GetAll()
        {
            throw new NotImplementedException();
        }

        //Exist method
        public bool OptionExists(string option)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Options.Any(e => e.Text.ToLower() == option.ToLower());
            }
        }

        public bool OptionIsAssigned(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                //Buscamos en questions
                //Returns true or false
                return context.Options.SingleOrDefault(e => e.Id == id).Questions.Count > 0;
            }
        }

        public bool Update(Option option)
        {
            throw new NotImplementedException();
        }
    }
}
