﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Collections.Generic;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IOptionRepository //
    {
        int Add(Option option);

        bool OptionExists(string option);

        Option Get(int optionId); //Get option by id

        ICollection<Option> GetAll();

        bool Delete(int id);

        bool OptionIsAssigned(int id);  //Assigned to a question

        bool Update(Option option);


        //bool Delete(int optionId);    Se hace desde el service

        //have not added update yet
    }
}
